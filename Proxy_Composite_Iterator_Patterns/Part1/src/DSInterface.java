
public interface DSInterface {

	/**
	 * thread safe insert
	 * @param index
	 */
	public void insert(Object o);
	
	/**
	 * thread safe remove
	 * @param index
	 */
	public void remove(Object o);
	
	/**
	 * thread safe get
	 * @param index
	 */
	public void get(int index);
	
}
