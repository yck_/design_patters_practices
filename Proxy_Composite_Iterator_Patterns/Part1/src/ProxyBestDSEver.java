import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ProxyBestDSEver implements DSInterface{
	
	/**
	 * wrapped ds
	 */
	private BestDSEver ds;
	
	/**
	 * Java read write lock structure. It allow multiple 
	 * thread to read as long as there are no write
	 */
	private ReadWriteLock rwlock = new ReentrantReadWriteLock();
	
	/**
	 * constructor
	 */
	public ProxyBestDSEver() {
		ds=new BestDSEver();
	}
	
	/**
	 * thread safe insert
	 * @param index
	 */
	public void insert(Object o) {
		rwlock.writeLock().lock();
		ds.insert(o);
		rwlock.writeLock().unlock();
	}
	
	/**
	 * thread safe remove
	 * @param index
	 */
	public void remove(Object o) {
		rwlock.writeLock().lock();
		ds.remove(o);
		rwlock.writeLock().unlock();
	}
	
	/**
	 * thread safe get
	 * @param index
	 */
	public void get(int index) {
		rwlock.readLock().lock();
		ds.get(index);
		rwlock.readLock().lock();
	}
	
}
