

public class Main {

	public static void main(String[] args) {
		ProxyBestDSEver ds=new ProxyBestDSEver();
		
		Thread thread1 = new Thread(new Runnable() {
	        @Override
	        public void run() {
	            for (int i = 0; i < 10000; i++) {
	                ds.insert("A");
	            }
	        }
	    });

	    Thread thread2 = new Thread(new Runnable() {
	        @Override
	        public void run() {
	            for (int i = 0; i < 10000; i++) {
	                ds.insert("B");
	            }
	        }
	    });
	    
	    
	    thread1.start();
	    thread2.start();

	}
  
}
