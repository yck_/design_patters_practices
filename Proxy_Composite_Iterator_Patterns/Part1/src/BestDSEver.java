
/**
 * Best DS Ever class
 * @author yusuf
 *
 */
public class BestDSEver implements DSInterface{
	
	public void insert(Object o) {
		System.out.println("Inserting...");
	}
	
	public void remove(Object o) {
		System.out.println("Removing...");
	}
	
	public void get(int index) {
		System.out.println("Getting...");
	}
	
	
}
