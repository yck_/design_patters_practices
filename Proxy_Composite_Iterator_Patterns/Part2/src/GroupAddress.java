import java.util.ArrayList;
import java.util.Iterator;

/**
 * Group address mail address list class
 * @author yusuf
 *
 */
public class GroupAddress extends MailAddressComponent{

	/**
	 * List of mail addresses belongs to same group
	 */
	private MailList mailAddressComponents=new MailList();
	
	/**
	 * name of group
	 */
	private String name;
	
	/**
	 * Mail address of group
	 */
	private String mailAddress;
	
	
	/**
	 * Constructor for group mail address
	 * @param name name of the user
	 * @param mailAdress mail address
	 */
	public GroupAddress(String name, String mailAddress) {
		this.name=name;
		this.mailAddress=mailAddress;
	}
	
	@Override
	public void add(MailAddressComponent mailAddressComponent) {
		// TODO Auto-generated method stub
		mailAddressComponents.add(mailAddressComponent);
	}
	
	@Override
	public void remove(MailAddressComponent mailAddressComponent) {
		mailAddressComponents.remove(mailAddressComponent);
	}
	
	@Override
	public MailAddressComponent getChild(int i) {
		return mailAddressComponents.get(i);
	}
	
	@Override
	public String getMailAddress() {
		return mailAddress;
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public void print() {
		System.out.println(mailAddress +" "+ name);
		
		Iterator iterator=mailAddressComponents.createIterator();
		while(iterator.hasNext()) {
			MailAddressComponent mailComponent=(MailAddressComponent) iterator.next();
			mailComponent.print();
		}
		
	}
	
	@Override
	public Iterator createIterator() {
		// TODO Auto-generated method stub
		return new CompositeIterator(mailAddressComponents.createIterator());
	}
	
}
