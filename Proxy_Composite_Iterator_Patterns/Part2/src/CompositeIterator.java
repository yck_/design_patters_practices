import java.util.Iterator;
import java.util.Stack;

public class CompositeIterator implements Iterator{
	
	/**
	 * Stack for printing tree structure
	 */
	private Stack stack=new Stack();
	
	/**
	 * Constructor
	 * @param iterator
	 */
	public CompositeIterator(Iterator iterator) {
		stack.push(iterator);
	}
	
	
	@Override
	public boolean hasNext() {
		if(stack.empty()) {
			return false;
		}
		else {
			Iterator iterator = (Iterator) stack.peek();
			
			if(!iterator.hasNext()) {
				stack.pop();
				return hasNext();
			}
			else {
				return true;
			}
		}
	}

	@Override
	public Object next() {
		if(hasNext()) {
			Iterator iterator=(Iterator) stack.peek();
			MailAddressComponent component = (MailAddressComponent) iterator.next();
			
			if(component instanceof GroupAddress) {
				stack.push(component.createIterator());
			}
			return component;
		}
		else {
			return null;		
		}

	}

}
