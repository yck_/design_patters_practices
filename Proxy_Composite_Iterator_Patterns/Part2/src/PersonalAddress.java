import java.util.Iterator;

public class PersonalAddress extends MailAddressComponent{

	private String mailAddress;
	private String name;
	
	/**
	 * Constructor for personal mail address
	 * @param name name of the user
	 * @param mailAdress mail address
	 */
	public PersonalAddress(String name, String mailAddress) {
		this.name=name;
		this.mailAddress=mailAddress;
	}

	@Override
	public String getMailAddress() {
		// TODO Auto-generated method stub
		return mailAddress;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public void print() {
		// TODO Auto-generated method stub
		System.out.println(mailAddress +" "+ name);
	}

	
	@Override
	public Iterator createIterator() {
		// TODO Auto-generated method stub
		return super.createIterator();
	}

	
	
}
