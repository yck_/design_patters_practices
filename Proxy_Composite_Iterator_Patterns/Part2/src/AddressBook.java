import java.util.Iterator;

public class AddressBook {
	
	MailAddressComponent addressBook;
	
	public AddressBook() {
		addressBook=new GroupAddress("Address Book", "");
	}
	
	public void add(MailAddressComponent component) {
		addressBook.add(component);
	}
	
	public void print() {
		addressBook.print();
	}
	
	public Iterator createIterator() {
		return addressBook.createIterator();
	}
}
