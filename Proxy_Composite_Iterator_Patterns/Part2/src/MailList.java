import java.util.ArrayList;

/**
 * This is a simple array list wrapper implementation for creating my own iterator. It basically uses array list operations.
 * @author yusuf
 *
 */
public class MailList{
	
	/**
	 * Array list 
	 */
	private ArrayList<MailAddressComponent> list;
	
	/**
	 * Constructor
	 */
	public MailList() {
		list=new ArrayList<MailAddressComponent>();
	}
	
	/**
	 * add
	 * @param mailAddressComponent
	 */
	public void add(MailAddressComponent mailAddressComponent) {
		list.add(mailAddressComponent);
	}
	
	/**
	 * remove
	 * @param mailAddressComponent
	 */
	public void remove(MailAddressComponent mailAddressComponent) {
		list.remove(mailAddressComponent);
	}
	
	/**
	 * iterator
	 * @return MailListIterator
	 */
	public MailListIterator createIterator() {
		return new MailListIterator(list);
	}
	
	/**
	 * get
	 * @param i index
	 * @return MailAddressComponent
	 */
	public MailAddressComponent get(int i) {
		return list.get(i);
	}
}
