import java.util.Iterator;

/**
 * Mail Adress component abstract class
 * @author yusuf
 *
 */
public abstract class MailAddressComponent {
	
	/**
	 * Add a new mail address
	 * @param mailAddressComponent
	 */
	public void add(MailAddressComponent mailAddressComponent) {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Remove the mail adress
	 * @param mailAddressComponent
	 */
	public void remove(MailAddressComponent mailAddressComponent) {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Get Child of the mail adress
	 * @param i
	 * @return
	 */
	public MailAddressComponent getChild(int i) {
		throw new UnsupportedOperationException();
	}	
	
	/**
	 * get e-mail adress
	 * @return
	 */
	public String getMailAddress() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * get name
	 * @return
	 */
	public String getName() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * print the addresses along with its name or list of addresses with names.
	 */
	public void print() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Iterator
	 * @return
	 */
	public Iterator createIterator() {
		return new NullIterator();
	}
}
