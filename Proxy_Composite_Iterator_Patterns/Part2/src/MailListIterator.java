import java.util.ArrayList;
import java.util.Iterator;

/**
 * Iterator class for group address
 * @author yusuf
 *
 */
public class MailListIterator implements Iterator {
	/**
	 * Array list of group address instances
	 */
	private ArrayList groupAddress;
	int position=0;
	
	/**
	 * constructor
	 * @param groupAddress
	 */
	public MailListIterator(ArrayList groupAddress) {
		this.groupAddress=groupAddress;
	}
	
	@Override
	public boolean hasNext() {
		if(position>=groupAddress.size()) {
			return false;
		}
		return true;
	}

	@Override
	public Object next() {
		position+=1;
		return groupAddress.get(position-1);
	}

}
