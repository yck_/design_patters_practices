
public class Main {

	public static void main(String[] args) {
		
		AddressBook addressBook=new AddressBook();
		
		MailAddressComponent p1=new PersonalAddress("Ali Velioglu", "ali@abc.com");
		MailAddressComponent p2=new PersonalAddress("Veli Alioglu", "veli@abc.com");
		MailAddressComponent p3=new PersonalAddress("Enes Durmaz", "enes@abc.com");
		MailAddressComponent p4=new PersonalAddress("Ahmet Saymaz", "ahmet@abc.com");
		MailAddressComponent p5=new PersonalAddress("Ali Sayar", "aliSayar@abc.com");
		MailAddressComponent p6=new PersonalAddress("Leyla Olak", "leyle@abc.com");
		MailAddressComponent p7=new PersonalAddress("Yusuf Can Kan", "yusufcan@abc.com");
		MailAddressComponent p8=new PersonalAddress("Mehmet Barkin", "mehmet@abc.com");
		MailAddressComponent p9=new PersonalAddress("Tunc Giril", "tunc@abc.com");
		MailAddressComponent p10=new PersonalAddress("Merve Uzun", "merve@abc.com");
		
		MailAddressComponent p11=new PersonalAddress("Ayse Uzunoglu", "ayseuzun@abc.com");
		MailAddressComponent p12=new PersonalAddress("Dilara Kisaoglu", "dilarakis@abc.com");
		MailAddressComponent p13=new PersonalAddress("Yunus Ortaoglu", "yunusorta@abc.com");
		MailAddressComponent p14=new PersonalAddress("Ibrahim Macit", "ibrahimmacit@abc.com");
		MailAddressComponent p15=new PersonalAddress("Erchan Aptoula", "erchanaptoula@abc.com");
		MailAddressComponent p16=new PersonalAddress("Ogulcan Kosar", "ogulcankosar@abc.com");
		MailAddressComponent p17=new PersonalAddress("Bora Kosmaz", "borakosmaz@abc.com");
		MailAddressComponent p18=new PersonalAddress("Baran Uzer", "baran@abc.com");
		MailAddressComponent p19=new PersonalAddress("Dilan Parlak", "dilan@abc.com");
		
		
		MailAddressComponent g1=new GroupAddress("Computer Engineering", "ce@abc.com");
		MailAddressComponent g2=new GroupAddress("Electronic Engineering", "ee@abc.com");
		MailAddressComponent g3=new GroupAddress("Enviroment Engineering", "env@abc.com");
		MailAddressComponent g4=new GroupAddress("Physic", "Physic@abc.com");
		MailAddressComponent g5=new GroupAddress("Mathematics", "math@abc.com");
		MailAddressComponent g6=new GroupAddress("Space Engineering", "se@abc.com");
		MailAddressComponent g7=new GroupAddress("Architecture", "arc@abc.com");
		
		/*Fill groups with persons*/
		g1.add(p1);
		g1.add(p2);
		g1.add(p3);
		
		g2.add(p4);
		
		g3.add(p6);
		g3.add(p7);
		g3.add(p8);
		
		g4.add(p9);
		
		g5.add(p10);
		g5.add(p11);
		
		/*Fill groups with groups*/
		g1.add(g2);
		g2.add(g3);
		g2.add(g4);
		g2.add(g5);
		
		g6.add(p12);
		g6.add(p13);
		
		g2.add(p5);
		g7.add(p14);
		
		
		/*Fill address book*/
		addressBook.add(g1);
		addressBook.add(p15);
		addressBook.add(p16);
		addressBook.add(g6);
		addressBook.add(p17);
		addressBook.add(p18);
		addressBook.add(p19);
		addressBook.add(g7);
		
		/*Print the address book*/
		addressBook.print();
	}

}
