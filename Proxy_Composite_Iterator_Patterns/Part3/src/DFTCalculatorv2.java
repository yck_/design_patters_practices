import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Synchronization with wait notify synchronized and notifyall
 * @author yusuf
 *
 */
public class DFTCalculatorv2 extends DFTCalculator{
	

	private ReentrantLock  mutex1 = new ReentrantLock();
	private Semaphore  s = new Semaphore(0);
	
	private int threadEndSumCount=0;
	
	public DFTCalculatorv2(Matrix m1, Matrix m2) {
		super(m1, m2);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void waitThreadsToSum() {
		
		/*for saving threadEndSumCount variable*/
		mutex1.lock();
		threadEndSumCount++;
		
		if(threadEndSumCount==4) {
			s.release();
		}
			
		mutex1.unlock();
		
		try { s.acquire();} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		s.release();

	}
}
