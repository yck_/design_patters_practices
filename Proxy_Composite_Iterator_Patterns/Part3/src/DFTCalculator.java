
public abstract class DFTCalculator {
	
	/**
	 * matrix 1
	 */
	private Matrix m1;
	
	/**
	 * matrix 2
	 */
	private Matrix m2;
	
	/**
	 * dimention of matrix
	 */
	private int size;
	
	/**
	 * result matrix will be stored in this.
	 */
	private ComplexNumber[][] resultSum;
	
	
	
	private Object mutex = new Object();
	
	private int threadIndex=0;
	
	ComplexNumber[][] resultDFT;

	/**
	 * consstructor
	 * @param m1 matrix 1
	 * @param m2 matrix 2
	 */
	public DFTCalculator(Matrix m1,Matrix m2) {
		this.m1=m1;
		this.m2=m2;
		this.size=m1.getDimention();
		resultSum=new ComplexNumber[size][size];
		resultDFT=new ComplexNumber[size][size];
	}
	
	
	public ComplexNumber[][] getResultDFT() {
		return resultDFT;
	}
	
	public Matrix getResultMatrix() {
		return new Matrix(resultDFT);
	}
	
	/**
	 * calculate method for template pattern
	 */
	public final void calculateDFTofSum() {
		Thread thread[] = new Thread[4];
		
		for(int i=0;i<4;i++) {
			thread[i] = new Thread(new Runnable() {
		        @Override
		        public void run() {
		        	int x1,x2=0;
		        	
		        	/*set thread parameters for matrix region*/
		        	synchronized (mutex) { /*thread protection*/
			        	switch(threadIndex) {
				        	case 0:
					        	x1=0;
					        	x2=0;
				        		break;
				        	case 1:
				        		x1=size/2;
				        		x2=0;
				        		break;
				        	case 2:
				        		x1=0;
				        		x2=size/2;
				        		break;
				        	default:
				        		x1=size/2;
				        		x2=size/2;
				        		break;
			        	}
			        	threadIndex+=1;
			        	
			        	System.out.println("Thread "+Thread.currentThread().getName()+" started sum process.");
		        	}
		        	/*sum part*/
		        	sum(x1,x2);
		        	
		        	synchronized (this) {
		        		System.out.println("Thread "+Thread.currentThread().getName()+" ended sum process.");
		        	}
		        	
		        	waitThreadsToSum();
		        	
		        	synchronized (this) {
		        		System.out.println("Thread "+Thread.currentThread().getName()+" started DFT calculation.");
		        	}
		        	
		        	calculateDFT(x1,x2);
		        	
		        	synchronized (this) {
		        		System.out.println("Thread "+Thread.currentThread().getName()+" ended DFT calculation.");
		        	}
		    		
		        }
		    });
		}
		
		/*start threads*/
		for(int i=0;i<4;i++) {
			thread[i].start();
		}

		for(int i=0;i<4;i++) {
			try {
				thread[i].join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	
	/**
	 * 
	 * @param i1 start of x coordinate
	 * @param i2 start if y coordinate
	 */
	public void sum(int i1,int i2) {
		for(int i=i1;i<i1+size/2;i++) {
			for(int j=i2;j<i2+size/2;j++) {
				resultSum[i][j]=ComplexNumber.add(m1.getMatrix()[i][j], m2.getMatrix()[i][j]);
			}
		}
	}
	
	/**
	 * Waits all threads to finish the summary part.
	 * This part is made for template design pattern.
	 */
	public abstract void waitThreadsToSum();
	
	public void calculateDFT(int i1,int i2){
		int size=m1.getDimention();		

		for(int i=i1;i<i1+size/2;i++) {
			for(int j=i2;j<i2+size/2;j++) {
				resultDFT[i][j]=calculate2DFormula(i,j,size);
			}
		}
	}
	
	public ComplexNumber calculate2DFormula(int u,int v,int size) {
		ComplexNumber result=new ComplexNumber(0,0);
		for(int i=0;i<size;i++) {
			for(int j=0;j<size;j++) {
				result=ComplexNumber.add(result,ComplexNumber.multiply((m1.getMatrix()[i][j]),WCalculations(u,v,i,j,size)));
			}
		}
		return result;
	}
	
	/**
	 * This function calculates the w part using the 
	 * 	e^jQ = cos(Q) + jsin(Q) formula.
	 * @param u
	 * @param v
	 * @param x
	 * @param y
	 * @param size
	 */
	public ComplexNumber WCalculations(int u,int v,int x,int y,int size) {
		double value = -2*3.14*( ((u*x)/size) + ((v*y)/size));
		return new ComplexNumber((int)Math.cos(value),(int)Math.sin(value));
	}
}
