
public class Main {

	public static void main(String[] args) {
		
		int size=200;
		
		ComplexNumber[][] mtrx=new ComplexNumber[size][size];
		ComplexNumber[][] mtrx2=new ComplexNumber[size][size];
		
		for(int i=0;i<size;i++) {
			for(int j=0;j<size;j++) {
				mtrx[i][j]=new ComplexNumber(Math.floor(Math.random() * 101),Math.floor(Math.random() * 101));
				mtrx2[i][j]=new ComplexNumber(Math.floor(Math.random() * 101),Math.floor(Math.random() * 101));
			}
		}
		
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		Matrix m1=new Matrix(mtrx2);
		Matrix m2=new Matrix(mtrx2);
		System.out.println("synchronized, wait, notify and notifyAll mechanisms part starts!");
		
		DFTCalculator dc=new DFTCalculatorv1(m1,m2);
		dc.calculateDFTofSum();
		
		System.out.println("synchronized, wait, notify and notifyAll mechanisms part ended!");
		
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		
		System.out.println("mutex(es) and monitor(s) mechanism part starts!");
		
		DFTCalculator dc2=new DFTCalculatorv2(m1,m2);
		dc2.calculateDFTofSum();
		
		System.out.println("mutex(es) and monitor(s) mechanism part part ended!");
		
		
		/*For printing result matrix please uncomment below.*/
		//Matrix res= dc2.getResultMatrix();
		
		//ComplexNumber[][] resMatrix=res.getMatrix();

			
	}

}
