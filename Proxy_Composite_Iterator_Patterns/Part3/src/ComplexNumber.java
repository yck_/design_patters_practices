
/**
 * This class represents the complex numbers and it includes complex number operations as static method
 * @author yusuf
 *
 */
public class ComplexNumber {
	
	/**
	 * real part
	 */
	private double real;
	
	/**
	 * imaginary part
	 */
	private double imaginary;
	
	/**
	 * Constructor
	 * @param real
	 * @param imaginary
	 */
	public ComplexNumber(double real, double imaginary)
	{
		this.real = real;
		this.imaginary = imaginary;
	}
	
	/**
	 * get real part
	 * @return int
	 */
	public double getReal() {
		return real;
	}
	
	/**
	 * set real part
	 * @param real
	 */
	public void setReal(double real) {
		this.real = real;
	}
	
	/**
	 * get img part
	 * @return int
	 */
	public double getImaginary() {
		return imaginary;
	}
	
	/**
	 * set real part
	 * @param imaginary
	 */
	public void setImaginary(double imaginary) {
		this.imaginary = imaginary;
	}
		
	/**
	 * Sums 2 complex number and returns new object
	 * @param n1 complex number
	 * @param n2 complex number
	 * @return new object of complex number
	 */
	public static ComplexNumber add(ComplexNumber n1, ComplexNumber n2)
	{
		return new ComplexNumber(n1.real+n2.real,n1.imaginary+n2.imaginary);
	}
	
	/**
	 * multiplies 2 complex number and returns new object
	 * @param n1 complex number
	 * @param n2 complex number
	 * @return new object of complex number
	 */
	public static ComplexNumber multiply(ComplexNumber n1, ComplexNumber n2)
	{
		return new ComplexNumber(n1.real*n2.real - n1.imaginary*n2.imaginary,n1.real*n2.imaginary + n1.imaginary*n2.real);
	}
	
}