
/**
 * Synchronization with wait notify synchronized and notifyall
 * @author yusuf
 *
 */
public class DFTCalculatorv1 extends DFTCalculator{
	
	private Object m1 = new Object(); /*Object for using synchronization*/
	private int threadEndSumCount=0;
	public DFTCalculatorv1(Matrix m1, Matrix m2) {
		super(m1, m2);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void waitThreadsToSum() {
		synchronized(m1) {
			if(threadEndSumCount==3) {
				m1.notifyAll();
			}
			else {
				try { 
					threadEndSumCount+=1;
					m1.wait(); } 
				catch (InterruptedException e) { }
			}
		}
	}

}
