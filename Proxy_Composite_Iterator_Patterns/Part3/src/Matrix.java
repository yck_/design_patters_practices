
/**
 * Represents the 2D matrix
 * @author yusuf
 *
 */
public class Matrix {
	
	/**
	 * Dimension of matrix
	 */
	private int n=0;
	
	private ComplexNumber[][] matrix;
	
	public Matrix(int n) {
		this.n=n;
		matrix = new ComplexNumber[n][n];
	}
	
	public Matrix(ComplexNumber[][] matrix) {
		this.n=(matrix[0]).length;
		this.matrix = matrix;
	}
	
	/**
	 * sums two matrix
	 * @param m1
	 * @param m2
	 * @param i1 start index of x
	 * @param i2 start index of y
	 * @return
	 */
	public Matrix add(Matrix m1,int i1,int i2) {
		if(this.n!=m1.getDimention()) {
			return null;
			//throw UnsuppoertedOperationException();
		}
		
		//ComplexNumber[][] m3 = new ComplexNumber[n][n];
		
		for(int i=i1;i<(i1+n/2);i++) {
			for(int j=i2;i<(i2+n/2);j++) {
				this.matrix[i][j]= ComplexNumber.add(this.matrix[i][j],(m1.getMatrix())[i][j]);
			}
		}
		//return new Matrix(m3);
		return this;
	}
	
	/**
	 * gets the dimention
	 */
	public int getDimention() {
		return n;
	}
	
	/**
	 * getter
	 * @param matrix
	 */
	public ComplexNumber[][] getMatrix() {
		return matrix;
	}
	
}
