package GameObjects;

import javax.imageio.ImageIO;
import java.util.Random;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.awt.image.ImageObserver;

public class Tile {
	
	private Color color;
	private int x,y;
	
	private BufferedImage asset;

	/**
	 * Constructor
	 * @param x x
	 * @param y y
	 * @param rand random object
	 */
	public Tile(int x,int y,Random rand){
		switch(rand.nextInt(3)) {
		case 0:
			color=Color.red;
			try {
				asset=ImageIO.read(getClass().getResource("../img/tile_2.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 1:
			color=Color.blue;
			try {
				asset=ImageIO.read(getClass().getResource("../img/tile_1.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		default:
			color=Color.green;
			try {
				asset=ImageIO.read(getClass().getResource("../img/tile_3.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}
		this.x=x;
		this.y=y;
	}
	
	/**
	 * draw tile
	 * @param g
	 * @param observer
	 */
	public void draw(Graphics g,ImageObserver observer) {
		g.drawImage(asset,x,y,observer);
	}
	
	/**
	 * move tile to given screen coordinate
	 * @param x new x coordinate 
	 * @param y new y coordinate
	 */
	public void move(int x,int y) {
		this.x=x;
		this.y=y;
	}
	
	/**
	 * getter for color
	 * @return Color object
	 */
	public Color color() {
		return color;
	}
	
	/**
	 * get x
	 * @return x
	 */
	public int getX() {
		return x;
	}

	/**
	 * get y
	 * @return y
	 */
	public int getY() {
		return y;
	}
	
	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	
}
