package Canvas;
import java.util.Random;
import java.awt.Graphics;
import CharacterProperties.*;
import CharacterProperties.Character;
import GameObjects.Tile;

import java.awt.Color;
import java.awt.image.ImageObserver;

/**
 * Upper canvas. Includes character properties.
 *
 */
public class UpperCanvas {
	
	private CharacterDesigner characterDesigner;
	private Character player[];
	private Character enemy[];
	
	private BottomCanvas bottomCanvas;

	/**
	 * Constructor
	 * @param rand random object
	 * @param bc bottom canvas
	 */
	public UpperCanvas(Random rand,BottomCanvas bc) {
		characterDesigner=new CharacterDesigner(rand,bc);
		player = new Character[3];
		enemy = new Character[3];
		this.bottomCanvas=bc;
		
		for(int i=0;i<3;i++) {
			player[i]=characterDesigner.createCharacter("player");
			player[i].setPlace(i*110+80,10);
			enemy[i]=characterDesigner.createCharacter("enemy");
			enemy[i].setPlace(i*110+80,80);
		}
	}
	
	/**
	 * getter for player character list
	 * @return player list
	 */
	public Character[] getPlayer() {
		return player;
	}
	
	/**
	 * getter for enemy character list
	 * @return enemy list
	 */
	public Character[] getEnemy() {
		return enemy;
	}

	/**
	 * draws the character and enemy
	 * @param g graphics object
	 * @param observer image observer
	 */
	public void draw(Graphics g,ImageObserver observer) {
		g.setColor(Color.pink);
		g.fillRect(0,0,640,150);
		for(int i=0;i<3;i++) {
			player[i].draw(g,observer);
			enemy[i].draw(g,observer);
		}	
	}
	
	/**
	 * Damages the proper player/enemy character with using given tile object.
	 * @param tile tile object
	 * @param character enemy or player as a string.
	 * @return game end statemet as int. 1 for game over,2 for reset,0 for not ended.
	 */
	public int damage(Tile tile,String character) {
		int column=((tile.getX()-35)/40)/3;
		
		if(character.equals("enemy")) {
			if(!player[column].isDead()) {
				bottomCanvas.writeTerminal("Enemy ");
				player[column].damage(tile.color());
				bottomCanvas.addFirsTerminalElement("player "+(column+1)+" \n");
				
				if(player[column].isDead()) {
					bottomCanvas.writeTerminal("Player "+(column+1)+" is dead...\n");
				}
			}
		}
		else{
			if(!enemy[column].isDead()) {
				bottomCanvas.writeTerminal("Player ");
				enemy[column].damage(tile.color());
				bottomCanvas.addFirsTerminalElement("enemy "+(column+1)+" \n");
				
				if(enemy[column].isDead()) {
					bottomCanvas.writeTerminal("Enemy "+(column+1)+" is dead...\n");
				}
			}
		}
		
		return isGameEnd();
	}
	
	/**
	 * Checks the game end statement. 
	 * @return 1 for game over,2 for reset,0 for not ended.
	 */
	public int isGameEnd() {
		if(player[0].isDead() && player[1].isDead() && player[2].isDead()) {
			bottomCanvas.writeTerminal("Game Over. You can close the game...\n");
			return 1;
			//System.exit(0);
		}
		
		if(enemy[0].isDead() && enemy[1].isDead() && enemy[2].isDead()) {
			bottomCanvas.writeTerminal("Player win. New Characters are creating....\n");
			for(int i=0;i<3;i++) {
				enemy[i]=characterDesigner.createCharacter("enemy");
				enemy[i].setPlace(i*110+80,80);
				
				if(player[i].isDead()) {
					player[i].reset();
					//player[i]=characterDesigner.createCharacter("player");
				}
				else {
					player[i].reset();
				}
			}
			return 2;
		}
		return 0;
	}

}
