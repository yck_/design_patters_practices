package Canvas;

import GameObjects.Tile;
import java.awt.Graphics;
import java.awt.image.ImageObserver;
import java.util.ArrayList;
import java.util.Random;
import java.util.LinkedList;
import java.util.Queue;


import java.awt.Point;

/**
 * Middle part of he screen
 *
 */
public class MiddleCanvas{
	
	private ArrayList<Tile> crushedTiles;

	private Tile tiles[][];
	private int clicked_x=-1;
	private int clicked_y=-1;
	private Random rand;
	private BottomCanvas bottomCanvas;

	/**
	 * Middle part of the game. Includes tiles board.
	 * @param rand random for generating.
	 */
	public MiddleCanvas(Random rand,BottomCanvas bc){
		this.rand=rand;
		crushedTiles=new ArrayList<Tile>();
		initTiles();
		crushedTiles.clear();
		this.bottomCanvas=bc;
	}
	
	/**
//	 * Initiliazes the tile boards.
	 */
	public void initTiles() {
		tiles=new Tile[6][9];
		for(int i=0;i<6;i++) {
			for(int j=0;j<9;j++) {
				tiles[i][j]=new Tile(j*40+35, i*40+150,rand);
			}
		}
			
		for(int i=0;i<6;i+=1) {
			for(int j=0;j<9;j+=1) {
				controlAfterMove(i,j);
			}
		}
	}
	
	/**
	 * draws the current status of Middle Canvas
	 * @param g Graphics object
	 * @param observer observer
	 */
	public void draw(Graphics g,ImageObserver observer) {
		for(int i=0;i<6;i++) {
			for(int j=0;j<9;j++) {
				if(tiles[i][j]!=null) {
					tiles[i][j].draw(g,observer);
				}
			}
		}
	}
	
	/**
	 * click event for gameboard
	 * @param x x coordinate of mouse
	 * @param y y coourdinate of mouse
	 * @return false for enemy turn, true for player turn
	 */
	public boolean click(int x,int y) {
		if(x>35 && y>150 && x<395 && y<390) {
			int column=(x-35)/40;
			int row=(y-150)/40;
			
			if(clicked_x==-1 && clicked_y==-1) {
				clicked_x=column;
				clicked_y=row;
			}
			else {
				if(isNear(clicked_x,clicked_y,column,row)) {
					swap(clicked_x,clicked_y,column,row);
					bottomCanvas.writeTerminal("Player played: ("+clicked_x+" "+clicked_y+") to ("+column+" "+row+")\n");
					controlAfterMove(clicked_y,clicked_x);
					controlAfterMove(row,column);
					clicked_x=-1;
					clicked_y=-1;
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * chack if given tiles are near to each other.
	 * @param x1 x coordinate of tile 1
	 * @param y1 y coordinate of tile 1
	 * @param x2 x coordinate of tile 2
	 * @param y2 y coordinate of tile 2
	 * @return returs true if near
	 */
	public boolean isNear(int x1,int y1,int x2,int y2) {
		if(x1==x2 && (y1==y2-1 || y1==y2+1)) {
			return true;
		}
		
		if(y1==y2 && (x1==x2-1 || x1==x2+1)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Swaps the given tiles.
	 * @param x1 x coordinate of tile 1
	 * @param y1 y coordinate of tile 1
	 * @param x2 x coordinate of tile 2
	 * @param y2 y coordinate of tile 2
	 */
	public void swap(int x1,int y1,int x2,int y2) {
		Tile temp;
		temp=tiles[y1][x1];
		tiles[y1][x1]=tiles[y2][x2];
		tiles[y2][x2]=temp;
		
		int temp1,temp2=0;
		temp1=tiles[y1][x1].getX();
		temp2=tiles[y1][x1].getY();
		
		tiles[y1][x1].move(tiles[y2][x2].getX(),tiles[y2][x2].getY());
		tiles[y2][x2].move(temp1,temp2);
	}
	
	/**
	 * Controls the crush tile possibilities on given coordinates.
	 * @param x x index of tile
	 * @param y y index of tile
	 */
	public void controlAfterMove(int x,int y) {
		Queue<Point> coordinates = controlCrush(x,y);
		
		fillNullPlaces(coordinates);

		Point iter= coordinates.poll();
		while(iter!=null) {
			Queue<Point> subCoordinates = controlCrush(iter.x,iter.y);
			fillNullPlaces(subCoordinates);
			coordinates.addAll(subCoordinates);
			iter=coordinates.poll();
		}
	}
	
	
	/**
	 * Fills the null places in the board.
	 * @param coordinates takes the coordinates of possible null places.
	 */
	public void fillNullPlaces(Queue<Point> coordinates) {
        for (Point item: coordinates) {
    		for(int i=item.x;i<5;i++) {
    			tiles[i][item.y]=tiles[i+1][item.y];
    			if(tiles[i][item.y]!=null) {
    				tiles[i][item.y].setY(i*40+150);
    			}
    		}
    		tiles[5][item.y]=null;
        }
        for(int i=0;i<6;i++) {
        	for(int j=0;j<9;j++) {
        		if(tiles[i][j]==null) {
        			tiles[i][j]=new Tile(j*40+35, i*40+150,rand);
        		}
        	}
        }
	}
																																																																																																																																																																								
	/**
	 * Controls if there is crushed or not with given indexes.
	 * @param x x index of tile
	 * @param y y index of tile
	 * @return crushed tile coordinates.
	 */
	public Queue<Point> controlCrush(int x,int y) {
		Queue<Point> coordinates = new LinkedList<Point>();

		if(x<5 && x>0 && tiles[x+1][y]!=null && tiles[x-1][y]!=null &&
				tiles[x][y].color().getRGB()==tiles[x+1][y].color().getRGB() && 
				tiles[x][y].color().getRGB()==tiles[x-1][y].color().getRGB()) {
			coordinates.addAll(cleanRight(x,y));
			coordinates.addAll(cleanLeft(x,y));
		}
		else if(x<4 && tiles[x+1][y]!=null && tiles[x+2][y]!=null &&
				tiles[x][y].color().getRGB()==tiles[x+1][y].color().getRGB() && 
				tiles[x][y].color().getRGB()==tiles[x+2][y].color().getRGB()) {
			coordinates.addAll(cleanRight(x,y));
		}
		else if(x>1 && tiles[x-1][y]!=null && tiles[x-2][y]!=null &&
				tiles[x][y].color().getRGB()==tiles[x-1][y].color().getRGB() && 
				tiles[x][y].color().getRGB()==tiles[x-2][y].color().getRGB()) {
			coordinates.addAll(cleanLeft(x,y));
		}
	
		if(y<8 && y>0 && tiles[x][y+1]!=null && tiles[x][y-1]!=null &&
				tiles[x][y].color().getRGB()==tiles[x][y+1].color().getRGB() && 
				tiles[x][y].color().getRGB()==tiles[x][y-1].color().getRGB()) {
			coordinates.addAll(cleanUp(x,y));
			coordinates.addAll(cleanDown(x,y));
		}
		else if(y<7 && tiles[x][y+1]!=null && tiles[x][y+2]!=null &&
				tiles[x][y].color().getRGB()==tiles[x][y+1].color().getRGB() && 
				tiles[x][y].color().getRGB()==tiles[x][y+2].color().getRGB()) {
			coordinates.addAll(cleanUp(x,y));
		}
		else if(y>1 && tiles[x][y-1]!=null && tiles[x][y-2]!=null &&
				tiles[x][y].color().getRGB()==tiles[x][y-1].color().getRGB() && 
				tiles[x][y].color().getRGB()==tiles[x][y-2].color().getRGB()) {
			coordinates.addAll(cleanDown(x,y));
		}
		
		if(coordinates.size()!=0) {
			tiles[x][y]=null;
			coordinates.add(new Point(x,y));
		}
		return coordinates;
	}
 	
 	/**
 	 * Crushes the right of the tile 
 	 * @param x x index of tile
 	 * @param y y index of tile
 	 * @return crushed tile coordinates
 	 */
 	public Queue<Point> cleanRight(int x,int y) {
 		int i=1;
 		Queue<Point> coordinates = new LinkedList<Point>();
		while(x+i<6 && tiles[x+i][y]!=null && (tiles[x][y].color().getRGB() == tiles[x+i][y].color().getRGB())){
			coordinates.add(new Point(x+i,y));
			crushedTiles.add(tiles[x+i][y]);
			tiles[x+i][y]=null;			
			i+=1;
		}
		return coordinates;
 	}
 	
 	/**
 	 * Crushes the left of the tile 
 	 * @param x x index of tile
 	 * @param y y index of tile
 	 * @return crushed tile coordinates
 	 */
 	public Queue<Point> cleanLeft(int x,int y) {
 		int i=1;
 		Queue<Point> coordinates = new LinkedList<Point>();
 		while(x-i>=0 && tiles[x-i][y]!=null && (tiles[x][y].color().getRGB() == tiles[x-i][y].color().getRGB())){
			coordinates.add(new Point(x-i,y));
			crushedTiles.add(tiles[x-i][y]);
			tiles[x-i][y]=null;
			i+=1;
		}
		return coordinates;
 	}
 	
 	/**
 	 * Crushes the above of the tile 
 	 * @param x x index of tile
 	 * @param y y index of tile
 	 * @return crushed tile coordinates
 	 */
 	public Queue<Point> cleanUp(int x,int y) {
 		int i=1;
 		Queue<Point> coordinates = new LinkedList<Point>();
		while(y+i<9 && tiles[x][y+i]!=null && (tiles[x][y].color().getRGB() == tiles[x][y+i].color().getRGB())){
			coordinates.add(new Point(x,y+i));
			crushedTiles.add(tiles[x][y+i]);
			tiles[x][y+i]=null;
			i+=1;
		}
		return coordinates;
 	}
 	
 	/**
 	 * Crushes the below of the tile 
 	 * @param x x index of tile
 	 * @param y y index of tile
 	 * @return crushed tile coordinates
 	 */
 	public Queue<Point> cleanDown(int x,int y) {
 		int i=1;
 		Queue<Point> coordinates = new LinkedList<Point>();
		while(y-i>=0 && tiles[x][y-i]!=null && (tiles[x][y].color().getRGB() == tiles[x][y-i].color().getRGB())){
			coordinates.add(new Point(x,y-i));
			crushedTiles.add(tiles[x][y-i]);
			tiles[x][y-i]=null;
			i+=1;
		}
		return coordinates;
 	}
 	
 	/**
 	 * controls if given move is valid on the board or not.
 	 * @param x x index of tile
 	 * @param y y index of tile
 	 * @param move movement style as int
 	 * @return
 	 */
 	public boolean controlMoveisValid(int x,int y,int move) {
 		if(move==0 && y==0) {
 			return false;
		}
		else if(move==1 && y==8){
			return false;
		}
		else if(move==2 && x==0){
			return false;
		}
		else if(move==3 && x==5){
			return false;
		}
		return true;
 		
 	}
 	
 	/**
 	 * This method makes computer to make one move.
 	 */
 	public void computerTurn() {
 		int x,y=0;
 		int move=0;
		x=rand.nextInt(6);
		y=rand.nextInt(9);
		move=rand.nextInt(4);
		
		while(!controlMoveisValid(x,y,move)) {
			x=rand.nextInt(6);
			y=rand.nextInt(9);
			move=rand.nextInt(4);
		}
		
		switch(move) {
		case 0: /*up*/
			playerMove(y,x,y-1,x);
			bottomCanvas.writeTerminal("Computer played: ("+x+" "+y+") to ("+x+" "+(y-1)+")\n");
			break;
		case 1: /*down*/
			playerMove(y,x,y+1,x);
			bottomCanvas.writeTerminal("Computer played:("+x+" "+y+") to ("+x+" "+(y+1)+")\n");
			break;
		case 2: /*left*/
			playerMove(y,x,y,x-1);
			bottomCanvas.writeTerminal("Computer played:("+x+" "+y+") to ("+(x-1)+" "+y+")\n");
			break;
		default:/*right*/
			playerMove(y,x,y,x+1);
			bottomCanvas.writeTerminal("Computer played:("+x+" "+y+") to ("+(x+1)+" "+y+")\n");
			break;
		}
 	}
 	 	
 	/**
 	 * makes move for player with respect to given x y board coordinates.
 	 * @param x1 x1
 	 * @param y1 y1
 	 * @param x2 x2
 	 * @param y2 y2
 	 */
 	public void playerMove(int x1,int y1,int x2,int y2) {
		swap(x1,y1,x2,y2);
		controlAfterMove(y1,x1);
		controlAfterMove(y2,x2);
 	}

 	/**
 	 * Checks if there is any crush in the board
 	 */
	public void controlBoard() {
		for(int i=0;i<6;i+=1) {
			for(int j=0;j<9;j+=1) {
				controlAfterMove(i,j);
			}
		}
	}

	/**
	 * getter for crushed tile list
	 * @return crushed Tile list as an arraylist
	 */
	public ArrayList<Tile> getCrushedTiles() {
		return crushedTiles;
	}
	
	/**
	 * When player wins the game thus method works and 
	 */
	public void reset() {
		clicked_x=-1;
		clicked_y=-1;
		crushedTiles.clear();
	}
}






