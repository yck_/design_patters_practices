package Canvas;
import java.awt.Color;
import java.awt.Font;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

import javax.swing.JTextArea;

/**
 * Bottom canvas includes terminal properties
 *
 */
public class BottomCanvas {
	
	private JTextArea textField;

	private Deque<String> terminal = new ArrayDeque<>();
	
	/**
	 * constructor
	 */
	public BottomCanvas() {
		initTextField();
	}
	
	/**
	 * getter for terminal text field
	 * @return
	 */
	public JTextArea getTextField() {
		return textField;
	}
	
	/**
	 * Initializes the terminal text field.
	 */
	public void initTextField() {
		for(int i=0;i<13;i++) {
			terminal.add("");
		}

        textField=new JTextArea();
        textField.setForeground(Color.white);
        textField.setBackground(Color.black);
        textField.setVisible(true);
        textField.setEditable(false);
        textField.setLineWrap(true);
        textField.setWrapStyleWord(true);
        textField.setBounds(0,390,450,225);
        textField.setFont(new Font("Book Antiqua",Font.BOLD|Font.ITALIC,13));
	}
	
	
	/**
	 * writes given string to game terminal
	 * @param s string
	 */
	public void writeTerminal(String s) {
		
		if(!(s.equals("Game Over. You can close the game...\n") && terminal.getFirst().equals("Game Over. You can close the game...\n"))) {
			terminal.removeLast();
			terminal.addFirst(s);
		}
	}
	
	/**
	 * Updates the terminal on the screen
	 */
	public void updateTerminal() {
		StringBuilder stringBuilder = new StringBuilder();
		
		Iterator<String> iterator=terminal.descendingIterator();
   
		while (iterator.hasNext()){
			stringBuilder.append(iterator.next());
		}

		textField.setText(stringBuilder.toString());
	}
	
	/**
	 * appends given string to first element of the terminal string array.
	 * @param x
	 */
	public void addFirsTerminalElement(String x) {
		terminal.addFirst(terminal.remove()+x);
	}
}
