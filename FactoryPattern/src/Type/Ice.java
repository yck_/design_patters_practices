package Type;
import java.awt.Color;

public class Ice implements Type {
	
	private double strength;
	private double agility;
	private double health;
	private Color color;
	
	public Ice(){
		strength=125;
		agility=75;
		health=100;
		color=Color.blue;
	}

	@Override
	public double strength() {
		return strength;
	}

	@Override
	public double agility() {
		return agility;
	}

	@Override
	public double health() {
		return health;
	}
	
	@Override
	public Color color() {
		return color;
	}
}
