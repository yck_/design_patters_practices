package Type;
import java.awt.Color;

public class Fire implements Type {

	
	private double strength;
	private double agility;
	private double health;
	private Color color;
	
	public Fire(){
		strength=100;
		agility=125;
		health=75;
		color=Color.red;
	}

	@Override
	public double strength() {
		return strength;
	}

	@Override
	public double agility() {
		return agility;
	}

	@Override
	public double health() {
		return health;
	}
	
	@Override
	public Color color() {
		return color;
	}
	
}
