package Type;
import java.awt.Color;

public class Nature implements Type{
	
	private double strength;
	private double agility;
	private double health;
	private Color color;
	
	public Nature(){
		strength=75;
		agility=100;
		health=125;
		color=Color.green;
	}

	@Override
	public double strength() {
		return strength;
	}

	@Override
	public double agility() {
		return agility;
	}

	@Override
	public double health() {
		return health;
	}
	
	@Override
	public Color color() {
		return color;
	}
}
