package Type;
import java.awt.Color;

public interface Type {

	/**
	 * Gets strength boost
	 * @return double strength
	 */
	public abstract double strength();
	
	/**
	 * Gets agility boost
	 * @return double agility
	 */
	public abstract double agility();
	
	/**
	 * Gets health
	 * @return double health
	 */
	public abstract double health();
	
	/**
	 * gets the color of the type
	 * @return
	 */
	public abstract Color color();
}
