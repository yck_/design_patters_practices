package Style;

/**
 * Valhalla style class
 *
 */
public class Valhalla implements Style{

	private double strength;
	private double agility;
	private double health;
	
	public Valhalla(){
		strength=1.3;
		agility=0.4;
		health=1.3;
	}

	@Override
	public double strength() {
		// TODO Auto-generated method stub
		return strength;
	}

	@Override
	public double agility() {
		// TODO Auto-generated method stub
		return agility;
	}

	@Override
	public double health() {
		// TODO Auto-generated method stub
		return health;
	}
}
