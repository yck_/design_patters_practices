package Style;

/**
 * Atlantis style class
 *
 */
public class Atlantis implements Style {
	
	private double strength;
	private double agility;
	private double health;
	
	public Atlantis(){
		strength=0.8;
		agility=1.2;
		health=1.2;
	}

	@Override
	public double strength() {
		// TODO Auto-generated method stub
		return strength;
	}

	@Override
	public double agility() {
		// TODO Auto-generated method stub
		return agility;
	}

	@Override
	public double health() {
		// TODO Auto-generated method stub
		return health;
	}
}
