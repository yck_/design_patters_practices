package Style;

/**
 * Underwild style class
 *
 */
public class Underwild implements Style{
	
	private double strength;
	private double agility;
	private double health;
	
	public Underwild(){
		strength=0.8;
		agility=1.6;
		health=0.8;
	}

	@Override
	public double strength() {
		// TODO Auto-generated method stub
		return strength;
	}

	@Override
	public double agility() {
		// TODO Auto-generated method stub
		return agility;
	}

	@Override
	public double health() {
		// TODO Auto-generated method stub
		return health;
	}
}
