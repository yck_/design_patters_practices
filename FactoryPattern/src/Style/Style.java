package Style;

public interface Style {
	
	/**
	 * Gets strength boost
	 * @return double strength
	 */
	public abstract double strength();
	
	/**
	 * Gets agility boost
	 * @return double agility
	 */
	public abstract double agility();
	
	/**
	 * Gets health
	 * @return double health
	 */
	public abstract double health();
}
