package Game;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import Canvas.*;
import GameObjects.Tile;

/**
 * Controls the general game flow and game inputs. Sets computer and player turns and 
 * makes the communication between above-below-middle canvases.
 * This class is in place of bridge between canvases.
 *
 */
public class GameController implements MouseListener, MouseMotionListener{
	
	private MiddleCanvas middleCanvas;
	private UpperCanvas upperCanvas;
	private boolean gameOver=false;
	
	private boolean isPlayerTurn=true;
	
	/**
	 * Conroller for the mouse input and the game.
	 * @param mc
	 * @param uc
	 */
	public GameController(MiddleCanvas mc,UpperCanvas uc){
		this.middleCanvas=mc;
		this.upperCanvas=uc;
	}

	/**
	 * Sets the current turn.
	 */
	public void changeTurn() {
		if(isPlayerTurn) {
			isPlayerTurn=false;
		}
		else {
			isPlayerTurn=true;
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		
		if(!gameOver) {
	
			int isGameEnd=0;
			if(isPlayerTurn && e.getButton()==MouseEvent.BUTTON1) {
				isPlayerTurn=middleCanvas.click(e.getX(), e.getY());
				if(!isPlayerTurn) {
					ArrayList<Tile> tiles=middleCanvas.getCrushedTiles();
					
					for(Tile t:tiles) {
						isGameEnd=upperCanvas.damage(t, "player");
						if(isGameEnd==2) {
							middleCanvas.reset();
							isPlayerTurn=true;
							tiles=null;
							break;
						}
						else if(isGameEnd==1) {
							gameOver=true;
						}
					}
				}
			}
			
			if(!isPlayerTurn) {
				middleCanvas.computerTurn();
				isPlayerTurn=true;
				
				ArrayList<Tile> tiles=middleCanvas.getCrushedTiles();
				
				for(Tile t:tiles) {
					isGameEnd=upperCanvas.damage(t, "enemy");
					if(isGameEnd==2) {
						break;
					}
					else if(isGameEnd==1) {
						gameOver=true;
					}
				}
			}
			
			middleCanvas.controlBoard();
		}
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	
}
