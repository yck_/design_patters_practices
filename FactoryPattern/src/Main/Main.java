package Main;

import javax.swing.SwingUtilities;
public class Main {
	
	
	public static void main(String[] args){

		Screen gameWindow=new Screen();
		
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                gameWindow.init();
            }
        });

		
	}

}
