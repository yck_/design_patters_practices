package Main;

import javax.swing.JLabel;
import javax.swing.JPanel;
import Canvas.*;
import CharacterProperties.Character;
import Game.GameController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Graphics;
import java.awt.Color;
import java.util.Random;
import javax.swing.Timer;

/**
 *
 * Includes panel properties.
 *
 */
@SuppressWarnings("serial")
public class GamePanel extends JPanel implements ActionListener {
	
	private UpperCanvas upperCanvas;
	private MiddleCanvas middleCanvas;
	private BottomCanvas bottomCanvas;
	private JLabel healthLabels[];
	private GameController gameController;
	private Timer timer;
	private Random rand;
	private JLabel boardLabels[];
	
	
//	private BufferedImage buttonAssets;

	/**
	 * Game panel for he game.
	 * @param width width
	 * @param height height
	 */
	public GamePanel(int width,int height) {
		this.setBounds(0, 0, width, height);
		this.setBackground(new Color(245,201,218));
		this.setDoubleBuffered(true);
		this.setFocusable(true);
		this.setLayout(null);
		
		rand=new Random();
		bottomCanvas=new BottomCanvas();
		upperCanvas=new UpperCanvas(rand,bottomCanvas);
		middleCanvas=new MiddleCanvas(rand,bottomCanvas);
		this.add(bottomCanvas.getTextField());
		initHealthLabels();
		
		gameController=new GameController(middleCanvas, upperCanvas);
		
		this.addMouseListener(gameController);
		this.addMouseMotionListener(gameController);
		
        timer = new Timer(25, this);
        timer.start();
	}

	    @Override
	    public void paintComponent(Graphics g) {
	        super.paintComponent(g);
	        upperCanvas.draw(g,this);
	        middleCanvas.draw(g,this);

	    }

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
			Character[] ch=upperCanvas.getPlayer();
			Character[] en=upperCanvas.getEnemy();
			
			for(int i=0;i<3;i++) {
				healthLabels[i].setText(" "+((int)(ch[i].getHealth())));
				healthLabels[i].setPreferredSize(getPreferredSize());
				healthLabels[i+3].setText(" "+((int)(en[i].getHealth())));
				healthLabels[i+3].setPreferredSize(getPreferredSize());
			}
			
			bottomCanvas.updateTerminal();
	        repaint();
		}
		
		/**
		 * Initializes the health bars.
		 */
		public void initHealthLabels() {
		
			healthLabels=new JLabel[6];
			for(int i=0;i<3;i++) {
				healthLabels[i]=new JLabel("HP:0");
				healthLabels[i+3]=new JLabel("HP:0");
				healthLabels[i].setForeground(Color.black);
				healthLabels[i+3].setForeground(Color.black);
				healthLabels[i].setVisible(true);
				healthLabels[i+3].setVisible(true);
				healthLabels[i].setBounds(i*110+92,52,30,30);
				healthLabels[i+3].setBounds(i*110+92,122,30,30);
				this.add(healthLabels[i]);
				this.add(healthLabels[i+3]);
			}
			
			boardLabels=new JLabel[2];
			boardLabels[0]=new JLabel("P:");
			boardLabels[0].setForeground(Color.black);
			boardLabels[0].setVisible(true);
			boardLabels[0].setBounds(40,20,30,30);
			this.add(boardLabels[0]);
			
			boardLabels[1]=new JLabel("E:");
			boardLabels[1].setForeground(Color.black);
			boardLabels[1].setVisible(true);
			boardLabels[1].setBounds(40,90,30,30);
			this.add(boardLabels[1]);
			
			
		}


}
