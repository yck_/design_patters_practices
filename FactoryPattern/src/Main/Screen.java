package Main;

import javax.swing.JFrame;

/**
 * Screen class includes panel and frame properties
 * @author yusuf
 *
 */
public class Screen {

		
	private JFrame frame;
	private GamePanel gamePanel;
	public final static int WIDTH=450;
	public final static int HEIGHT=650;

	Screen(){ }
	
	/**
	 * initializes game window and panel
	 */
	public void init() {
		frame = new JFrame("GAME");
		frame.setSize(WIDTH,HEIGHT);
		frame.setResizable(false);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		gamePanel = new GamePanel(WIDTH,HEIGHT);
		frame.add(gamePanel);
		
	}
	
}
