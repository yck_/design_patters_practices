package CharacterProperties;

import Style.Style;
import Style.Valhalla;
import Type.Ice;
import Type.Type;

public class CharacterIceValhalla implements CharacterPropertiesFactory {

	@Override
	public Style createStyle() {
		return new Valhalla();
	}

	@Override
	public Type createType() {
		return new Ice();
	}

}
