package CharacterProperties;

import java.lang.Math;

import Canvas.BottomCanvas;
import Style.*;
import Type.Type;
import java.awt.image.BufferedImage;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.image.ImageObserver;

public abstract class Character {
	
	protected boolean isDead=false;
	protected Type type;
	protected Style style;
	
	protected double strength;
	protected double agility;
	protected double health;
	
	protected int x=0,y=0;
	protected BufferedImage asset;
	private BottomCanvas bottomCanvas;
	public Character(BottomCanvas bc){
		this.bottomCanvas=bc;
	}
	
	/**
	 * Applies damage to the character using its type and style attributes.
	 * @param color
	 */
	public void damage(Color color) {
	
		if(!isDead) {
				
			double damage=100* Math.pow((strength/agility),1.35);
			
			if(color.getRGB()==Color.red.getRGB()) {
				if(type.color().getRGB()==Color.green.getRGB()) {
					damage=damage*2;
				}
				else if(type.color().getRGB()==Color.blue.getRGB()) {
					damage=damage/2;
				}
			}
			else if(color.getRGB()==Color.blue.getRGB()) {
				if(type.color().getRGB()==Color.red.getRGB()) {
					damage=damage*2;
				}
				else if(type.color().getRGB()==Color.green.getRGB()) {
					damage=damage/2;
				}
			}
			else {
				if(type.color().getRGB()==Color.blue.getRGB()) {
					damage=damage*2;
				}
				else if(type.color().getRGB()==Color.red.getRGB()) {
					damage=damage/2;
				}
			}
			
			bottomCanvas.addFirsTerminalElement("hit " +(int)damage+" damage to ");
	
			health-= damage;
			
			if(health<=0) {
				isDead=true;
				health=0;
			}
		}
	}
	
	/**
	 * draws the character to the screen
	 * @param g Graphiocs object
	 * @param observer observer
	 */
	public void draw(Graphics g,ImageObserver observer) {
        g.setColor(type.color());
        g.fillRect(x, y, 50, 50);
        
        if(style instanceof Valhalla) {
        	g.setColor(Color.lightGray);
        }
        else if(style instanceof Underwild){
        	g.setColor(Color.orange);
        }
        else {
        	g.setColor(Color.magenta);
        }
        
        g.fillRect(x+10, y+10, 30, 30);
	}
	
	/**
	 * Initialization for strength,agility and health
	 */
	public void init() {
		strength=type.strength()*style.strength();
		agility=type.agility()*style.agility();
		health=type.health()*style.health();
	}
	
	/**
	 * resets the character health
	 */
	public void reset() {
		health=type.health()*style.health();
		isDead=false;
	}
	
	/**
	 * changes character coordinate
	 * @param x x coordinate
	 * @param y y coordinate
	 */
	public void setPlace(int x,int y) {
		this.x=x;
		this.y=y;
	}
	
	/**
	 * getter for character health
	 * @return health as double
	 */
	public double getHealth() {
		return health;
	}
	
	/**
	 * returns if character is ndead or not
	 * @return boolean
	 */
	public boolean isDead() {
		return isDead;
	}

}
