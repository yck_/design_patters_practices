package CharacterProperties;

import Style.Style;
import Style.Underwild;
import Type.Nature;
import Type.Type;

public class CharacterNatureUnderwild implements CharacterPropertiesFactory {

	@Override
	public Style createStyle() {
		return new Underwild();
	}

	@Override
	public Type createType() {
		return new Nature();
	}

}
