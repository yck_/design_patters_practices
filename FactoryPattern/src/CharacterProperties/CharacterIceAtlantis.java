package CharacterProperties;

import Style.Atlantis;
import Style.Style;
import Type.Ice;
import Type.Type;

public class CharacterIceAtlantis implements CharacterPropertiesFactory{

	@Override
	public Style createStyle() {
		return new Atlantis();
	}

	@Override
	public Type createType() {
		return new Ice();
	}

}
