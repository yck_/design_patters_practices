package CharacterProperties;
import java.awt.Graphics;

import Canvas.BottomCanvas;

/**
 * enemy character
 *
 */
public class EnemyCharacter extends Character{
	CharacterPropertiesFactory factory;
	
	EnemyCharacter(CharacterPropertiesFactory factory,BottomCanvas bc){
		super(bc);
		this.factory=factory;
		init();
		super.init();
	}
	
	/**
	 * Initialize the character properties
	 * 
	 */
	public void init(){
		type=factory.createType();
		style=factory.createStyle();
	}
	
	/**
	 * draw character to screen
	 */
	public void draw(Graphics g) {
		//g.drawImage(asset,x,y,null);
		
        g.setColor(type.color());
        g.fillRect(x, y, 50, 50);
	}
}
