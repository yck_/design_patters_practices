package CharacterProperties;

import Style.Style;
import Style.Valhalla;
import Type.Fire;
import Type.Type;

public class CharacterFireValhalla implements CharacterPropertiesFactory {

	@Override
	public Style createStyle() {
		return new Valhalla();
	}

	@Override
	public Type createType() {
		return new Fire();
	}

}
