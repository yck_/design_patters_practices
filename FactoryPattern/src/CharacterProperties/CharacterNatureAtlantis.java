package CharacterProperties;

import Style.Atlantis;
import Style.Style;
import Type.Nature;
import Type.Type;

public class CharacterNatureAtlantis implements CharacterPropertiesFactory {

	@Override
	public Style createStyle() {
		return new Atlantis();
	}

	@Override
	public Type createType() {
		// TODO Auto-generated method stub
		return new Nature();
	}

}
