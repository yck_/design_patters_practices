package CharacterProperties;

import Style.Style;
import Style.Underwild;
import Type.Ice;
import Type.Type;

public class CharacterIceUnderwild implements CharacterPropertiesFactory {

	@Override
	public Style createStyle() {
		return new Underwild();
	}

	@Override
	public Type createType() {
		return new Ice();
	}

}
