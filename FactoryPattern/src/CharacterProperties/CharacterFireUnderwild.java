package CharacterProperties;

import Style.Style;
import Style.Underwild;
import Type.Fire;
import Type.Type;

public class CharacterFireUnderwild implements CharacterPropertiesFactory {

	@Override
	public Style createStyle() {
		return new Underwild();
	}

	@Override
	public Type createType() {
		return new Fire();
	}

}
