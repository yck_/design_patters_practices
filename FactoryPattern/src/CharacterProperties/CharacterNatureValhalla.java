package CharacterProperties;

import Style.Style;
import Style.Valhalla;
import Type.Nature;
import Type.Type;

public class CharacterNatureValhalla implements CharacterPropertiesFactory {

	@Override
	public Style createStyle() {
		return new Valhalla();
	}

	@Override
	public Type createType() {
		return new Nature();
	}

}
