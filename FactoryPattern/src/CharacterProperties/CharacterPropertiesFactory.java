package CharacterProperties;

import Style.Style;
import Type.Type;

public interface CharacterPropertiesFactory {
	public Style createStyle();
	public Type createType();
	
}
