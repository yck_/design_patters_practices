package CharacterProperties;

import java.util.Random;

import Canvas.BottomCanvas;

/**
 * Creates a new character
 *
 */
public class CharacterDesigner {

	Random rand;
	BottomCanvas bottomCanvas;
	
	/**
	 * Constructor
	 * @param rand random object
	 * @param bc BottomCanvas object for communicating with the terminal
	 */
	public CharacterDesigner(Random rand,BottomCanvas bc) {
		this.rand=rand;
		bottomCanvas=bc;
		
	}
	
	/**
	 * Creates a new character randomly.
	 * @param characterType
	 * @return
	 */
	public Character createCharacter(String characterType) {
		Character character=null;
		CharacterPropertiesFactory characterFactory=null;
		
		switch(rand.nextInt(9)) {
		case 0:
			characterFactory=new CharacterIceAtlantis();
			break;
		case 1:
			characterFactory=new CharacterIceValhalla();
			break;
		case 2:
			characterFactory=new CharacterIceUnderwild();
			break;
		case 3:
			characterFactory=new CharacterFireAtlantis();
			break;
		case 4:
			characterFactory=new CharacterFireValhalla();
			break;
		case 5:
			characterFactory=new CharacterFireUnderwild();
			break;
		case 6:
			characterFactory=new CharacterNatureAtlantis();
			break;
		case 7:
			characterFactory=new CharacterNatureValhalla();
			break;
		default:
			characterFactory=new CharacterNatureUnderwild();
		}
		
		if(characterType.equals("enemy")) {
			character=new EnemyCharacter(characterFactory,bottomCanvas);
		}
		else {
			character=new PlayerCharacter(characterFactory,bottomCanvas);
		}
		return character;
	}
}
