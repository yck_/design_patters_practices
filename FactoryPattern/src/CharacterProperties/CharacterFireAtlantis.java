package CharacterProperties;

import Style.Atlantis;
import Style.Style;
import Type.Fire;
import Type.Type;

public class CharacterFireAtlantis implements CharacterPropertiesFactory {

	@Override
	public Style createStyle() {
		return new Atlantis();
	}

	@Override
	public Type createType() {
		return new Fire();
	}

}
