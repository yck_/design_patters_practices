This repository was created for practicing design patterns.

The repository includes examples for;
 - Factory Pattern
 - Synchronized Proxy Pattern
 - Composite Pattern
 - Iterator Pattern
 - Template Pattern
 - etc.